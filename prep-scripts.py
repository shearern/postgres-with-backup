import os, sys
from textwrap import dedent

ENTRYPOINT_SCRIPT = sys.argv[1]

WINDOWS_ENDLINE = b'\r\n'
LINUX_ENDLINE = b'\n'


def dos2unix(path):
    print(f"dos2unix {path}")
    with open(path, 'rb') as fh:
        content = fh.read()
    with open(path, 'wb') as fh:
        fh.write(content.replace(WINDOWS_ENDLINE, LINUX_ENDLINE))


if __name__ == '__main__':

    ADD_BEFORE_LINE = '# then restart script as postgres user'
    ADD_LINES = dedent("""\
        # Start backup loop as root
        echo "Starting backup loop"
        /usr/local/bin/backup-loop.sh &    
        """)

    # Add backup loop to entrypoint
    print(f"Reading {ENTRYPOINT_SCRIPT}")
    lines = list()
    found = False
    with open(ENTRYPOINT_SCRIPT, 'rt') as fh:
        for line in fh.read().split("\n"):
            if line.strip() == ADD_BEFORE_LINE:
                print(f"Found token line.  Inserting code to start backup loop")
                fount = True
                for add_line in ADD_LINES.split("\n"):
                    lines.append("\t"*3 + add_line)
            lines.append(line)
    if not found:
        print(f"ERROR: Failed to find token line: {add_line}")

    print(f"Writing {ENTRYPOINT_SCRIPT}")
    with open(ENTRYPOINT_SCRIPT, 'wt') as fh:
        fh.write("\n".join(lines))

    # Perform line-ending changes
    dos2unix('/usr/local/bin/backup-loop.sh')

    print("Finished")