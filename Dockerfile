# =========================================================
# Extract the entrypoint for the upstream image
# =========================================================
FROM postgres:15.3 as upstream

# =========================================================
# Patch upstream entrypoint script
# =========================================================
FROM python:3 as patcher

COPY --from=upstream /usr/local/bin/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
COPY backup-loop.sh /usr/local/bin/backup-loop.sh
COPY prep-scripts.py /usr/local/bin/prep-scripts.py

RUN python3 /usr/local/bin/prep-scripts.py /usr/local/bin/docker-entrypoint.sh


# =========================================================
# Build postgres image with modfied scripts
# =========================================================
FROM postgres:15.3

COPY --from=patcher /usr/local/bin/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
COPY --from=patcher /usr/local/bin/backup-loop.sh       /usr/local/bin/backup-loop.sh

RUN chmod 775 /usr/local/bin/backup-loop.sh

RUN mkdir -p /var/backup-vol
VOLUME /var/backup-vol

ENV BACKUP_EVERY_MIN=60
ENV BACKUP_PATH=/var/backup-vol/db_backup.gz

# Copied from upstream Dockerfile
# ref: https://github.com/docker-library/postgres/blob/a23c0e97980edae5be2cd4eb68ff1f0762d031cd/15/bullseye/Dockerfile
VOLUME /var/lib/postgresql/data
ENTRYPOINT ["docker-entrypoint.sh"]
STOPSIGNAL SIGINT
EXPOSE 5432
CMD ["postgres"]
