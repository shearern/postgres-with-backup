"""Test Postgres server is up"""
import os, sys
import unittest
import logging
from datetime import datetime, timedelta

import psycopg2
import backoff


logging.basicConfig(level=logging.INFO)


def getenv(name):
    try:
        return os.environ[name]
    except KeyError:
        print(f"ERROR: Set ${name}")
        sys.exit(1)


@backoff.on_exception(backoff.constant, psycopg2.OperationalError, max_time=60, jitter=None, interval=10)
def connect():
    return psycopg2.connect(
        host =      getenv('POSTGRES_HOST'),
        user =      getenv('POSTGRES_USER'),
        password =  getenv('POSTGRES_PASSWORD'),
        database =  getenv('POSTGRES_DB'),
        )


class FileNotReady(Exception): pass

backup_interval = timedelta(minutes=int(getenv('BACKUP_EVERY_MIN')))

@backoff.on_exception(backoff.constant, FileNotReady, interval=10, jitter=None,
                      max_time=(backup_interval*2).total_seconds())
def wait_for_backup_file_exists():

    path = getenv("BACKUP_PATH")

    if not os.path.exists(path):
        raise FileNotReady(f"{path} doesn't exist")
    
    age = datetime.now() - datetime.fromtimestamp(os.path.getmtime(path))

    if age > backup_interval:
        raise FileNotReady(f"{path} exists, but is {age} old")


class TestPostgresServer(unittest.TestCase):


    def test_connects(self):
        connect()


    def test_backup_appears(self):
        wait_for_backup_file_exists()



if __name__ == '__main__':
    unittest.main()
