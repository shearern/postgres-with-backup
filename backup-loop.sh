#!/bin/bash

function abort
{
    echo "ERROR: $1"
    exit 2
}


# Sanity Checks
if [ -z "$BACKUP_EVERY_MIN" ]; then
    abort 'BACKUP_EVERY_MIN is required'
fi
if [ -z "$BACKUP_PATH" ]; then
    abort 'BACKUP_PATH is required'
fi
if [ -z "$POSTGRES_DB" ]; then
    abort '$POSTGRES_DB is required'
fi
if [ -z "$POSTGRES_USER" ]; then
    abort '$POSTGRES_USER is required'
fi
BACKUP_DIRECTORY=`dirname "$BACKUP_PATH"`
if [ ! -d "$BACKUP_DIRECTORY" ]; then
    abort "Backup directory doesn't exist: $BACKUP_DIRECTORY"
fi


BACKUP_PAUSE_SECONDS=$((60*$BACKUP_EVERY_MIN))

if [ "$1" == "--check" ]; then
    exit 0
else
    echo "Backing up to $BACKUP_PATH every $BACKUP_EVERY_MIN minutes ($BACKUP_PAUSE_SECONDS seconds)"

    while [ 1 ]; do

        sleep $BACKUP_PAUSE_SECONDS

        echo "Backing up to $BACKUP_PATH"
        if ! /usr/bin/pg_dumpall -U "$POSTGRES_USER" | gzip > "$BACKUP_PATH.tmp"; then
            abort "Backup failed"
        else
            if ! mv "$BACKUP_PATH.tmp" "$BACKUP_PATH"; then
                abort "Failed to move backup file to $BACKUP_PATH"
            fi
        fi

    done

fi

