Postgres DB Container with Backup Loop
======================================

PostgreSQL DB container with a backup loop added in.

images.spokanebaptist.net/postgres-with-backup:15


    db:
      image: images.spokanebaptist.net/postgres-with-backup:15
      environment:
        POSTGRES_USER:      appdb
        POSTGRES_PASSWORD:  appdb
        POSTGRES_DB:        appdb
        BACKUP_EVERY_MIN:   1440
        BACKUP_PATH:        /var/db-backup
      volumes:
       - db_data:/var/lib/postgresql/data
       - db_backup:/var/db-backup


Specify:

 - BACKUP_EVERY_MIN: Frequency to backup in minutes
 - BACKUP_PATH: Path to .gz file to dump backup to

