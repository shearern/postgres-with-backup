"""Calculate build number from release branch name"""
import os, sys
import re
import subprocess
import string

RELEASE_BRANCH_PAT=re.compile(r'^(release|hotfix)\/([^\/]+)$')


def abort(msg, code=2):
    print(f"ERROR: {msg}")
    sys.exit(code)


def set_teamcity_parm(name, value):
    print(f"##teamcity[setParameter name='env.{name}' value='{value}']")


def print_parm(name, value):
    pad_name = (name + ':').ljust(20)
    print(f"{pad_name} {value}")


if __name__ == '__main__':

    # Usage: 
    argv = [arg for arg in sys.argv[1:] if not arg.startswith("-")]
    if len(argv) == 1:
        commit = argv[0]
        branch = None
    elif len(argv) > 1:
        commit, branch = argv[0:2]
    else:
        abort(f"Usage: {os.path.basename(sys.argv[0])} commit [branch] [--teamcity]")

    print_parm("COMMIT", commit)
    print_parm("BRANCH", branch)

    version = None
    major_version = None
    minor_version = None
    build_version = None

    # Parse branch name
    if branch and RELEASE_BRANCH_PAT.match(branch):
        version = RELEASE_BRANCH_PAT.match(branch).group(2)

    # Get tag from commit
    if not version:
        version = subprocess.check_output(f'git describe --abbrev=0 --tags {commit}'.split()).decode('ascii').strip()

    if not version:
        abort(f"Couldn't calculate a version for commit")

    # Calc Versions
    parts = version.split(".")
    if re.compile(r'\d+').match(parts[0]):
        major_version = parts[0]

        if len(parts) >= 2:
            minor_version = '.'.join(parts[:2])
        else:
            minor_version = f"{major_version}.0"

        if len(parts) >= 3:
            build_version = '.'.join(parts[:3])
        else:
            build_version = f"{minor_version}.0"

    else:
        abort("Major version is not a number")

    # Print Versions
    print_parm("IMAGE_MAJOR_VERSION", major_version)
    print_parm("IMAGE_MINOR_VERSION", minor_version)
    print_parm("IMAGE_BUILD_VERSION", build_version)

    if '--teamcity' in sys.argv:
        set_teamcity_parm("IMAGE_VERSION", version)
        set_teamcity_parm("IMAGE_MAJOR_VERSION", major_version)
        set_teamcity_parm("IMAGE_MINOR_VERSION", minor_version)
        set_teamcity_parm("IMAGE_BUILD_VERSION", build_version)
        
    if '--teamcity' in sys.argv and branch == 'master':
        print(f"##teamcity[buildNumber '{version}']")

    
